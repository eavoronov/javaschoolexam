package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int height = 0;
        int width = 0;
        int count = 1;
        int temp = inputNumbers.size();

        while (temp != 0) {
            temp -= count;
            count++;
            if (temp < 0)
                throw new CannotBuildPyramidException();
            else
                height++;
        }
        width = 2 * height - 1;
        count = 0;
        int[][] result = new int[height][width];
        try {
            Collections.sort(inputNumbers);
            for (int i = 0; i < height; i++) {
                for (int j = 0; j < 2 * i + 1; j += 2) {
                    result[i][height - 1 + j - i] = inputNumbers.get(count);
                    count++;
                }
            }
        } catch (OutOfMemoryError | NullPointerException e) {
            throw new CannotBuildPyramidException();
        }
        return result;
    }


}
