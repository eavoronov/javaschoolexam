package com.tsystems.javaschool.tasks.calculator;


import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Util {

    /*Конвертация в обратную польскую запись*/
    static List<String> convert(String statement) {

        List<String> result = new ArrayList<>();
        Stack<Character> temp = new Stack<>();
        char tempchar;
        int parCount = 0;

        for (int i = 0; i < statement.length(); i++) {
            char c = statement.charAt(i);

            /*Если цифра, то "собираем" все число (может быть из нескольких разрядов, включая точку)*/
            if (Character.isDigit(c)) {
                StringBuilder sb = new StringBuilder(String.valueOf(c));
                int j = i;
                while ((j != statement.length() - 1) && (Character.isDigit(statement.charAt(j + 1)) || statement.charAt(j + 1) == '.')) {
                    sb.append(String.valueOf(statement.charAt(j + 1)));
                    i++;
                    j++;
                }
                result.add(sb.toString());
                continue;
            }

            switch (c) {
                case '+':
                case '-':
                case '*':
                case '/':
                    if (i == statement.length() - 1)
                        return null; //Оператор не может быть последним символом выражения
                    if (temp.isEmpty()) {
                        temp.push(c);
                        break;
                    } else {
                        while (!temp.isEmpty() && priority(c) <= priority(temp.peek())) {
                            result.add(String.valueOf(temp.pop()));
                        }
                        temp.push(c);
                    }
                    break;
                case '(':
                    parCount++;
                    if (i == statement.length() - 1)
                        return null; //Оператор не может быть последним символом выражения
                    temp.push(c);
                    break;
                case ')':
                    parCount--;
                    do {
                        if (temp.isEmpty())
                            return null; //Разное количество открывающих и закрывающих скобок
                        tempchar = temp.pop();
                        if (tempchar != '(')
                            result.add(String.valueOf(tempchar));
                    } while (tempchar != '(');
                    break;
                default:
                    return null; //Неподходящий символ в выражении
            }
        }
        if (parCount != 0)
            return null; //Разное количество скобок
        while (!temp.isEmpty()) {
            result.add(String.valueOf(temp.pop()));
        }
        return result;
    }

    private static int priority(Character c) {
        if (c == '(') return 0;
        if (c == '+' || c == '-') return 1;
        return 2;
    }
}
