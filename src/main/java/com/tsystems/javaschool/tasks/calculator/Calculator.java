package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement == "")
            return null;
        List<String> converted = Util.convert(statement);
        if (converted == null)
            return null;
        Stack<Double> stack = new Stack<>();

        for (int i = 0; i < converted.size(); i++) {
            if (Character.isDigit(converted.get(i).charAt(0))) {
                try {
                    stack.push(Double.parseDouble(converted.get(i)));
                } catch (NumberFormatException e) {
                    return null;
                }
            } else {
                if (stack.size() < 2)
                    return null;
                double one = stack.pop();
                double two = stack.pop();
                char c = converted.get(i).charAt(0);
                switch (c) {
                    case '+':
                        stack.push(two + one);
                        break;
                    case '-':
                        stack.push(two - one);
                        break;
                    case '*':
                        stack.push(two * one);
                        break;
                    case '/':
                        if (one == 0)
                                return null;
                        stack.push(two / one);
                        break;
                    default:
                        return null;
                }
            }

        }
        double result = stack.pop();
        if (result % 1 != 0) {
            NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
            DecimalFormat df = (DecimalFormat)nf;
            df.setMaximumFractionDigits(4);
            return df.format(result);
        } else {
            return String.valueOf((int)result);
        }
    }

}
